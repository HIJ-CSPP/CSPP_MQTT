#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Copyright 2022 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
Published under EUPL."""

import datetime
import os
import platform
import queue
import re
import sys
import threading
import time
import paho.mqtt.client as mqtt

broker="ee-raspi1001.gsi.de"
hostname = platform.node()
client_name = hostname +"_"+ re.split(".py", os.path.basename(__file__))[0]
date_time = re.split(" ",str(datetime.datetime.now()))
client_id = client_name #+ "_" + date_time[0] + "_" + date_time[1]
topic_command = client_id+"/Command"
topic_status = client_id+"/Status"
DeviceActor_topics = "CSPP_Core_SV/+"
DeviceActor_Reset= "CSPP_Core_SV/DeviceActor_Reset"
DeviceActor_SelfTest= "CSPP_Core_SV/DeviceActor_SelfTest"

q = queue.Queue()

def on_log(client, userdata, level, buf):
        print("log: "+buf)

def on_connect(client, userdata, flags, rc):
    if rc==0:
        client.connected_flag = True
        print("Connected OK")
        publish_all(client)
        subscribe_all(client)
    else:
        print("Bad connection, returned code =",rc)

def on_disconnect(client, userdata, flags, rc=0):
    #client.connected_flag = False
    print("Disconnected result code ="+str(rc))

def on_publish(client, userdata, mid):
    #print("Client published message ID =", mid)
    pass

def on_message(client,userdata,msg):
    topic=msg.topic
    m_decode=str(msg.payload.decode("utf-8","ignore"))
    #print("Message received. Topic:", topic,"Payload:",m_decode)
    q.put((topic,m_decode))

def on_subscribe(client, userdata, mid, granted_qos):
    #print("Client subscribed message ID =", mid, "with qos =",granted_qos)
    pass

def on_unsubscribe(client, userdata, mid):
    #print("Client unsubscribed message ID =", mid)
    pass
            
def publish_all(client):
    #publish topics here;
    rc, mid = client.publish(client_id,"connected")
    print("Publishing: 'connected' returned rc =", rc, "mid = ", mid)
    rc, mid = client.publish(topic_status,0)
    print("Publishing: '",topic_status,"' returned rc =", rc, "mid = ", mid)
    rc, mid = client.publish(topic_command,"Write your command here.")
    print("Publishing: '",topic_command,"' returned rc =", rc, "mid = ", mid)

def subscribe_all(client):
    #subscribe to topics here;
    rc, mid = client.subscribe(topic_command)
    print("Subscribing to: '",topic_command,"'returned rc =", rc, "mid = ", mid)
    rc, mid = client.subscribe(DeviceActor_topics)
    print("Subscribing to: '",DeviceActor_topics,"'returned rc =", rc, "mid = ", mid)

def unsubscribe_all(client):
    #unsubscribe from topics here;
    rc, mid = client.subscribe(topic_command)
    print("Unsubscribing to: '",topic_command,"'returned rc =", rc, "mid = ", mid)

def command_processor():
    #implement command processing here;
    while True:
        received = q.get()
        topics = re.split("/", received[0])
        #print("Topics: ", topics)
        data = received[1]
        DeviceActor_topics = re.split("DeviceActor_", topics[1])
        #print("DeviceActor Topics: ", DeviceActor_topics)
        #print("Command processing:", topics[1])
        if topics[1] == "Command":
            print(topics[1], data)
        elif DeviceActor_topics[0] == "":
            #print(DeviceActor_topics[1], data)
            print(topics[1], data)
        else:
            print("Received unkown:", received[0])
        #print("Command processing done.")
        q.task_done()

def periodic_processor(client, ii):
        client.publish(topic_status,ii)
        client.publish(DeviceActor_SelfTest,"true")
        pass
        
if __name__ == '__main__':
    print(os.path.basename(__file__),"\n\
        Copyright 2021 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
        Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
        eMail: H.Brand@gsi.de\n\
        Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n\
        Published under EUPL.")
    # Insert you main code here
    try:
        #client = mqtt.Client(client_id="", clean_session=True, userdata=None, protocol=MQTTv311, transport="tcp")
        client = mqtt.Client(client_id, clean_session=True)
        client.connected_flag = False
        client.will_set(client_id,"Offline",1,False)
        #bind call back function
        client.on_connect=on_connect
        client.on_disconnect=on_disconnect
        #client.on_log=on_log
        client.on_publish=on_publish
        client.on_message=on_message
        client.on_subscribe=on_subscribe
        client.on_unsubscribe=on_unsubscribe
        print("Connecting client '",client_id, " 'to broker",broker)
        # turn-on the worker thread
        threading.Thread(target=command_processor, daemon=True).start()
        client.loop_start()
        client.connect(broker, port=1883, keepalive=60, bind_address="")
        while not client.connected_flag:
            print("Waiting for",broker,"...")
            time.sleep(1)
        time.sleep(3)
        print("Waiting for exception (Ctrl+c) ...")
        ii = 0
        while client.connected_flag:
            ii += 1
            periodic_processor(client, ii)
            #client.publish(topic_status,ii)
            time.sleep(10)
                
    except BaseException as e:
        print("Exception catched!", e)
        client.connected_flag = False
    finally:
        unsubscribe_all(client)
        q.join()
        print("Publishing: 'disconnected'")
        rc, mid = client.publish(client_id,"disconnected")
        print("Publishing: 'disconnected' returned rc =", rc, "mid = ", mid)
        print("Disonnecting from broker",broker)
        client.disconnect()
        time.sleep(1)
        print("Stopping message loop")
        client.loop_stop(force=False)
        print("Script execution stopped.")
        sys.exit()
